import React from 'react';

export default class NewTransactionForm extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            amount: 0,
            bankId: props.bankId
        };
    }

    componentWillReceiveProps(props) {
        this.setState({ bankId: props.bankId });
    }

    render() {
        const banks = this.props.banks.map(bank => {
            return <option value={bank.id}>{bank.name}</option>;
        });

        const maxAmount = 999999999999999;

        return (
            <div className='new-transaction-form'>
              <div className='new-transaction-form-row'>
                <div className='new-transaction-form-label'>Банк</div>
                <select className='new-transaction-form-input'
                        value={this.state.bankId}
                        onChange={e => {
                            this.setState({ bankId: +e.target.value });
                        }}>
                  {banks}
                </select>
              </div>
              <div className='new-transaction-form-row'>
                <div className='new-transaction-form-label'>Сумма $</div>
                <input className='new-transaction-form-input'
                       type='number'
                       value={this.state.amount}
                       min='0'
                       onChange={e => {
                           const value = e.target.value;
                           const amount = +`${value}`.replace(/[^0-9]/g, '');

                           if (amount <= maxAmount) {
                               this.setState({ amount });
                           }
                       }}/>
              </div>
              <div className='new-transaction-buttons-group'>
                <button className='pure-button pure-button-primary'
                        disabled={(
                            this.state.amount === 0 ||
                            this.state.bankId === null
                        )}
                        onClick={() => this.props.onTransaction({
                            bankId: this.state.bankId,
                            amount: this.state.amount
                        })}>
                  Перевести деньги
                </button>
                <button className='pure-button button-cancel'
                        onClick={() => this.props.onCancel()}>
                  Отмена
                </button>
              </div>
            </div>
        );
    }
}
