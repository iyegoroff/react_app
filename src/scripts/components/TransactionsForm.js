import React from 'react';
import find from 'lodash.find';

export default class TransactionsForm extends React.Component {

    render() {
        const transactions = this.props.transactions.map(transaction => {
            const bank = find(this.props.banks, { id: transaction.bankId });

            return (
                <div className='transactions-form-row'>
                  <div className='transactions-form-info'>
                    <div className='transactions-form-bank-name'>
                      {bank ? bank.name : 'Неизвестный банк'}
                    </div>
                    <div className='transactions-form-amount'>
                      {transaction.amount} $
                    </div>
                  </div>
                  <div className='transactions-form-button-wrapper'>
                    <button className='pure-button button-delete'
                            onClick={() => {
                                this.props.onTransactionDelete(transaction);
                            }}>
                      Удалить
                    </button>
                  </div>
                </div>
            );
        });

        return (
            <div className='transactions-form'>
              <div className='transactions-form-row'>
                <div className='transactions-form-header'>
                  <div className='transactions-form-bank-name-label'>Банк</div>
                  <div className='transactions-form-amount-label'>Сумма</div>
                </div>
              </div>
              {transactions}
              <div className='transactions-form-row'>
                <div className='transactions-form-spacer'></div>
                <div className='transactions-form-button-wrapper'>
                  <button className='pure-button pure-button-primary'
                          onClick={() => this.props.onTransactionCreate()}>
                    Добавить
                  </button>
                </div>
              </div>
            </div>
        );
    }
}
