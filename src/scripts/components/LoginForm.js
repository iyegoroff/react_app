import React from 'react';

export default class LoginForm extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            name: '',
            pass: ''
        };
    }

    logIn() {
        this.props.onLogIn(this.state.name, this.state.pass);
    }

    isLogInEnabled() {
        return this.state.name && this.state.pass;
    }

    render() {
        return (
            <div className='login-form'>
              <input className='login-input'
                     value={this.state.name}
                     onChange={e => this.setState({ name: e.target.value })}
                     placeholder='Имя пользователя' />
              <input className='login-input'
                     value={this.state.pass}
                     onChange={e => this.setState({ pass: e.target.value })}
                     onKeyPress={e => {
                         if (e.charCode === 13 && this.isLogInEnabled()) {
                             this.logIn();
                         }
                     }}
                     placeholder='Пароль'
                     type='password' />
              <button className='pure-button pure-button-primary'
                      onClick={() => this.logIn()}
                      disabled={!this.isLogInEnabled()}>
                Войти
              </button>
            </div>
        );
    }
}
