import React from 'react';
import { Link } from 'react-router'

export default class NavigationMenu extends React.Component {

    render() {
        const activeLink = 'pure-button-active navigation-menu-active-link';

        return (
            <div className='navigation-menu'>
              <Link className='pure-button navigation-menu-link'
                    activeClassName={activeLink}
                    to='/transactions'>
                Все транзакции
              </Link>
              <Link className='pure-button navigation-menu-link'
                    activeClassName={activeLink}
                    to='/new_transaction'>
                Добавить транзакцию
              </Link>
              <div className='navigation-menu-exit-wrapper'>
                <Link className='pure-button navigation-menu-link'
                      to='/'>
                  Выход
                </Link>
              </div>
            </div>
        );
    }
}
