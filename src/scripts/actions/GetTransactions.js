import { backendUrl } from '../config';
import { handleAjaxError, handleAjaxResponse } from '../utils';

const getTransactions = () => dispatch => (
    fetch(`${backendUrl}/transactions`)
        .then(handleAjaxResponse)
        .then(transactions => dispatch({
            type: 'GET_TRANSACTIONS',
            transactions
        }))
        .catch(handleAjaxError)
);

export default getTransactions;
