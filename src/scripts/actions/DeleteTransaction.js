import { backendUrl } from '../config';
import { deleteJson, handleAjaxError, handleAjaxResponse } from '../utils';

const deleteTransaction = transaction => dispatch => (
    fetch(`${backendUrl}/transactions?id=${transaction.id}`)
        .then(handleAjaxResponse)
        .then(ts => ts.length
            ? deleteJson(`${backendUrl}/transactions`, transaction)
            : null
        )
        .then(() => dispatch({
            type: 'DELETE_TRANSACTION',
            transaction
        }))
        .catch(handleAjaxError)
);

export default deleteTransaction;
