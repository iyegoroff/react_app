import { backendUrl } from '../config';
import { postJson, handleAjaxError, handleAjaxResponse } from '../utils';

const addTransaction = transaction => dispatch => (
    postJson(`${backendUrl}/transactions`, transaction)
        .then(handleAjaxResponse)
        .then(t => dispatch({
            type: 'ADD_TRANSACTION',
            transaction: t
        }))
        .catch(handleAjaxError)
);

export default addTransaction;
