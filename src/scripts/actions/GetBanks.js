import { backendUrl } from '../config';
import { handleAjaxError, handleAjaxResponse } from '../utils';

const getBanks = () => dispatch => (
    fetch(`${backendUrl}/banks`)
        .then(handleAjaxResponse)
        .then(banks => dispatch({
            type: 'GET_BANKS',
            banks
        }))
        .catch(handleAjaxError)
);

export default getBanks;
