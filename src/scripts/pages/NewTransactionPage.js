import React from 'react';
import { connect } from 'react-redux';
import { hashHistory } from 'react-router';
import { handleAjaxError, handleAjaxResponse } from '../utils';
import addTransaction from '../actions/AddTransaction';
import NewTransactionForm from '../components/NewTransactionForm';
import NavigationMenu from '../components/NavigationMenu';

class NewTransactionPage extends React.Component {

    render() {
        const banks = this.props.banks;

        return (
            <div className='page'>
              <NavigationMenu />
              <div className='form-wrapper'>
                <NewTransactionForm banks={banks}
                                    bankId={banks.length ? banks[0].id : null}
                                    onTransaction={t => {
                                        this.props.dispatch(addTransaction(t))
                                            .then(() => {
                                                hashHistory.push('/transactions');
                                            });
                                    }}
                                    onCancel={() => {
                                        hashHistory.push('/transactions');
                                    }}/>
              </div>
            </div>
        );
    }
}

NewTransactionPage = connect(state => ({
    banks: state.banks
}))(NewTransactionPage);

export default NewTransactionPage;
