import React from 'react';
import { hashHistory } from 'react-router';
import { handleAjaxError, handleAjaxResponse } from '../utils';
import { backendUrl } from '../config';
import LoginForm from '../components/LoginForm';

export default class LoginPage extends React.Component {

    render() {
        return (
            <div className='form-wrapper'>
              <LoginForm onLogIn={(name, pass) => {
                fetch(`${backendUrl}/profile`)
                    .then(handleAjaxResponse)
                    .then(json => {
                        if (json.name === name && json.pass === pass) {
                            hashHistory.push('/transactions');

                        } else {
                            alert(
                                'Введен неправильный пароль и/или имя пользователя.'
                            );
                        }
                    })
                    .catch(handleAjaxError);
            }}/>
          </div>
        );
    }
}
