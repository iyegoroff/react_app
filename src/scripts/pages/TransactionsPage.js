import React from 'react';
import { hashHistory } from 'react-router';
import { connect } from 'react-redux';
import { handleAjaxError, handleAjaxResponse } from '../utils';
import getTransactions from '../actions/GetTransactions';
import deleteTransaction from '../actions/DeleteTransaction';
import TransactionsForm from '../components/TransactionsForm';
import NavigationMenu from '../components/NavigationMenu';

class TransactionsPage extends React.Component {

    componentDidMount() {
        this.props.dispatch(getTransactions());
    }

    render() {
        return (
            <div className='page'>
              <NavigationMenu />
              <div className='form-wrapper'>
                <TransactionsForm transactions={this.props.transactions}
                                  banks={this.props.banks}
                                  onTransactionDelete={t => {
                                      this.props.dispatch(deleteTransaction(t));
                                  }}
                                  onTransactionCreate={() => {
                                      hashHistory.push('/new_transaction');
                                  }}/>
              </div>
            </div>
        );
    }
}

TransactionsPage = connect(state => ({
    transactions: state.transactions,
    banks: state.banks
}))(TransactionsPage);

export default TransactionsPage;
