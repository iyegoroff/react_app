require('../content/imports.scss');
require('object.assign').shim();

import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, hashHistory } from 'react-router';
import { Provider } from 'react-redux';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import transactions from './reducers/Transactions';
import banks from './reducers/Banks';
import getBanks from './actions/GetBanks';
import LoginPage from './pages/LoginPage';
import NewTransactionPage from './pages/NewTransactionPage';
import TransactionsPage from './pages/TransactionsPage';

const store = createStore(
    combineReducers({ transactions, banks }),
    applyMiddleware(thunk)
);

store.dispatch(getBanks());

ReactDOM.render(
    <Provider store={store}>
      <Router history={hashHistory}>
        <Route path='/' component={LoginPage}></Route>
        <Route path='/new_transaction' component={NewTransactionPage}></Route>
        <Route path='/transactions' component={TransactionsPage}></Route>
      </Router>
    </Provider>,
    document.getElementById('app')
);
