const banks = (state = [], action) => {
    switch (action.type) {
        case 'GET_BANKS':
            return action.banks;
        default:
            return state;
    }
};

export default banks;
