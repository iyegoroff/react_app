const transactions = (state = [], action) => {
    switch (action.type) {
        case 'GET_TRANSACTIONS':
            return action.transactions;
        case 'ADD_TRANSACTION':
            return [...state, action.transaction];
        case 'DELETE_TRANSACTION':
            return state.filter(t => t.id !== action.transaction.id);
        default:
            return state;
    }
};

export default transactions;
