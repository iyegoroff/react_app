export function handleAjaxResponse(response) {
    return response.ok ? response.json() : Promise.reject(response);
}

export function handleAjaxError(response) {
    if (response instanceof Response) {
        alert('Внутренняя ошибка сервера.');
    }
}

export function postJson(url, json) {
    const params = {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(json)
    };

    return fetch(url, params);
}

export function deleteJson(url, json) {
    return fetch(`${url}/${json.id}`, { method: 'DELETE' });
}
